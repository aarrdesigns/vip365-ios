//
//  WMDetailMethoodPaymentTableViewCell1.swift
//  vip365
//
//  Created by Wilmer Mendoza on 4/10/17.
//  Copyright © 2017 Andres Rodriguez. All rights reserved.
//

import UIKit

class WMDetailMethoodPaymentTableViewCell: UITableViewCell {

    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        setupView()
    }
        
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let titleLabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.clipsToBounds = true
        label.text = "Numero de tarjeta"
        label.textColor = UIColor.placeHolderColor()
        return label
    }()
        
    let numberCardLabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.clipsToBounds = true
        label.text = "••••9959"
        return label
    }()

    let separatorView : UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.separatorCell()
        return view
    }()
        
    func setupView(){
    
        addSubview(titleLabel)
        addSubview(numberCardLabel)
        addSubview(separatorView)
        
        titleLabel.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -10).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 24).isActive = true
            
        numberCardLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 2).isActive = true
        numberCardLabel.leftAnchor.constraint(equalTo: titleLabel.leftAnchor, constant: 0).isActive = true
        numberCardLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -8).isActive = true
        
        separatorView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        separatorView.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        separatorView.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        separatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
    }
}
