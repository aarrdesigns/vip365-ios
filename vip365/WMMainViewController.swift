//
//  WMMainViewController.swift
//  vip365
//
//  Created by Wilmer Mendoza on 2/10/17.
//  Copyright © 2017 Andres Rodriguez. All rights reserved.
//

import UIKit
import GoogleMaps

class WMMainViewController: UIViewController {
    
    let menuLauncher = WMMenuLauncher()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUI()
    }
    
    fileprivate func setUI(){
        
        //CREATING UI ELEMENTS
        let menuButton : UIButton = {
            let button = UIButton(type: .system)
            button.translatesAutoresizingMaskIntoConstraints = false
            button.clipsToBounds = true
            button.setImage(#imageLiteral(resourceName: "menu").withRenderingMode(.alwaysTemplate), for: .normal)
            button.tintColor = .black
            button.addTarget(self, action: #selector(showMenu), for: UIControlEvents.touchDown)
            return button
        }()
        
        let whereToView : UIView = {
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.layer.shadowPath = UIBezierPath(rect: view.bounds).cgPath
            view.layer.shouldRasterize = true
            view.backgroundColor = .white
            return view
        }()
        
        let squareView : UIView = {
            let square = UIView()
            square.translatesAutoresizingMaskIntoConstraints = false
            square.clipsToBounds = true
            square.backgroundColor = .black
            square.dropShadow(color: .red, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)

            return square
        }()
        
        let whereToTextField : UITextField = {
            let textField = UITextField()
            textField.translatesAutoresizingMaskIntoConstraints = false
            textField.clipsToBounds = true
            let str = NSAttributedString(string: "Where to?", attributes: [NSForegroundColorAttributeName:UIColor.black])
            textField.attributedPlaceholder = str
            textField.borderStyle = .none
            textField.font = UIFont.systemFont(ofSize: 16, weight: UIFontWeightLight)
            return textField
        }()
        
        let verticalSeparatorView : UIView = {
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.clipsToBounds = true
            view.backgroundColor = UIColor.separatorCell()
            return view
        }()
        
        let carImage : UIImageView = {
            let image = UIImageView()
            image.translatesAutoresizingMaskIntoConstraints = false
            image.clipsToBounds = true
            image.image = #imageLiteral(resourceName: "taxi").withRenderingMode(.alwaysTemplate)
            image.tintColor = UIColor.lightGray
            image.contentMode = .scaleAspectFill
            return image
        }()
        
        // Create a GMSCameraPosition that tells the map to display the
        // coordinate -33.86,151.20 at zoom level 6.
        let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 6.0)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        mapView.isMyLocationEnabled = true
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
        marker.title = "Sydney"
        marker.snippet = "Australia"
        marker.map = mapView
        
        for gesture in mapView.gestureRecognizers! {
            mapView.removeGestureRecognizer(gesture)
        }
        
        view = mapView
        //ADDING UI ELEMENTS TO THE VIEW WITH THEIR CONSTRAINTS
        view.addSubview(menuButton)
        view.addSubview(whereToView)
        view.addSubview(squareView)
        view.addSubview(whereToTextField)
        view.addSubview(carImage)
        view.addSubview(verticalSeparatorView)
        
        
        mapView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        mapView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        mapView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        mapView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true

        menuButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 40).isActive = true
        menuButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 24).isActive = true
        menuButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        menuButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        whereToView.topAnchor.constraint(equalTo: menuButton.bottomAnchor, constant: 55).isActive = true
        whereToView.leftAnchor.constraint(equalTo: menuButton.leftAnchor, constant: 0).isActive = true
        whereToView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -24).isActive = true
        whereToView.heightAnchor.constraint(equalToConstant: 55).isActive = true
        
        squareView.centerYAnchor.constraint(equalTo: whereToView.centerYAnchor, constant: 0).isActive = true
        squareView.leftAnchor.constraint(equalTo: whereToView.leftAnchor, constant: 24).isActive = true
        squareView.heightAnchor.constraint(equalToConstant: 6).isActive = true
        squareView.widthAnchor.constraint(equalToConstant: 6).isActive = true
        
        whereToTextField.centerYAnchor.constraint(equalTo: whereToView.centerYAnchor, constant: 0).isActive = true
        whereToTextField.leftAnchor.constraint(equalTo: squareView.rightAnchor, constant: 18).isActive = true
        whereToTextField.rightAnchor.constraint(equalTo: whereToView.rightAnchor, constant: -60).isActive = true
    
        carImage.centerYAnchor.constraint(equalTo: whereToView.centerYAnchor, constant: 0).isActive = true
        carImage.rightAnchor.constraint(equalTo: whereToView.rightAnchor, constant: -16).isActive = true
        carImage.widthAnchor.constraint(equalToConstant: 20).isActive = true
        carImage.heightAnchor.constraint(equalToConstant: 20).isActive = true

        verticalSeparatorView.topAnchor.constraint(equalTo: whereToView.topAnchor, constant: 11).isActive = true
        verticalSeparatorView.bottomAnchor.constraint(equalTo: whereToView.bottomAnchor, constant: -11).isActive = true
        verticalSeparatorView.rightAnchor.constraint(equalTo: carImage.leftAnchor, constant: -12).isActive = true
        verticalSeparatorView.widthAnchor.constraint(equalToConstant: 1).isActive = true
        
    }
    
    //MARK: METODO QUE SE USA PARA MOSTRAR EL DRAWER
    func showMenu(){
        menuLauncher.mainViewController = self
        menuLauncher.showSettings()
    }
    

}
