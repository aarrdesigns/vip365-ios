//
//  WMMenuLauncher.swift
//  vip365
//
//  Created by Wilmer Mendoza on 2/10/17.
//  Copyright © 2017 Andres Rodriguez. All rights reserved.
//

import UIKit

class WMMenuLauncher: NSObject {

    let cellId1 = "cellId1"
    let cellId2 = "cellId2"
    let cellId3 = "cellId3"

    var mainViewController: WMMainViewController?
    
    override init() {
        super.init()
        
        //SETTING COLLECTIONVIEW
        collectionView.dataSource = self
        collectionView.delegate = self
        
        collectionView.register(WMHeaderMenuCollectionViewCell.self, forCellWithReuseIdentifier: cellId1)
        collectionView.register(WMBodyMenuCollectionViewCell.self, forCellWithReuseIdentifier: cellId2)
        
    }
    
    //esta es la vista q se mostrara semitransparente sobre toda la ventana de la aplicacion
    let blackview = UIView() // vista negra opaca que aparece cuando se hace tap en el boton de menu

    
    //collectionView que servira para hacer el menuDrawer
    let collectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.rgb(red: 238, green: 238, blue: 238)
        cv.bounces = false
        return cv
    }()
    
    //este arreglo contiene las opciones que se desplegaran en las celdas
    let menuOption : [WMMenu] = {
        return[WMMenu(name: "Pagos"),WMMenu(name: "Tus viajes"),WMMenu(name: "Ayuda"),WMMenu(name: "Configuracion")]
    }()

    
    //metodo para el boton de 3 rayas
    func showSettings ()  {
        
        self.collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .top, animated: false)
        
        self.blackview.isHidden = false
        
        //var q me permite guardar toda la ventana de la app
        if let window = UIApplication.shared.keyWindow{
            
            blackview.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
            //agregandole la funcionalidad de dismiss cuando se haga tap en la ventana
            blackview.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(WMMenuLauncher.handlerMiss)))

            
            //agregando la subVista blackView, whiteView a la ventana
            window.addSubview(blackview)
            window.addSubview(collectionView)
            
            //esta es el ancho q tendra la collection
            let width : CGFloat = window.frame.width * 0.80
            
            //estoy haciendo q la coordenada en x quede al raz con el bottom de la ventana
            let x = width - window.frame.width
            // print(window.frame.height)
//            print(x)
            
            //seteando el collection view para q quede fuera de la ventana en el eje de las y negativas
            collectionView.frame = CGRect(x: x, y: 0, width: width , height: window.frame.height )
            collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 75, right: 0)
            
            //haciendo q el blackview cubra toda la ventana
            blackview.frame = CGRect(x: 0, y: 0 , width: window.frame.width, height: self.collectionView.frame.height)
            blackview.alpha = 0
            
            UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
                self.blackview.alpha = 1
                
                self.collectionView.frame = CGRect(x: 0, y: 0, width: self.collectionView.frame.width, height: self.collectionView.frame.height)
            }, completion: nil)
        }
        
    }
    
    
    //funcion que se usa para ocultar el menu de navegacion lateral y toda la logica y eventos que ello conlleva
    func handlerMiss()  {
        print("hizo tap sobre la vista negra ")
        
        UIView.animate(withDuration: 0.5, animations: {
            
            self.blackview.alpha = 0
            self.blackview.isHidden = true
                        
            if let window = UIApplication.shared.keyWindow{
                let x = (window.frame.width) * -1
                
                //volviendo a ocultar el collectionview
                self.collectionView.frame = CGRect(x: x, y: 0 , width: self.collectionView.frame.width, height: self.collectionView.frame.height)
            }
        })
    }
    
}


extension WMMenuLauncher: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate{

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId1, for: indexPath) as! WMHeaderMenuCollectionViewCell
        let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: cellId2, for: indexPath) as! WMBodyMenuCollectionViewCell
        
        if indexPath.row == 0{
            return cell
        }else{
            cell2.setting = menuOption[indexPath.row - 1]
            return cell2
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch indexPath.row {
        case 0:
            return CGSize(width: collectionView.frame.size.width, height: 110)
        default:
            return CGSize(width: collectionView.frame.size.width, height: 45)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //haciendo q el menu desaparesca al hacerle tap a una celda y navegar al controller seleccionado
        UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            //Aqui se implementa todo lo que tiene q ver con la desaparicion del menu
            
            self.blackview.alpha = 0
            
            if let window = UIApplication.shared.keyWindow{
                let x = (window.frame.width) * -1
                //volviendo a ocultar el collectionview
                self.collectionView.frame = CGRect(x: x, y: 0, width: collectionView.frame.width, height: collectionView.frame.height)
            }
            
            
        }) { (completed: Bool) in
            //aqui va todo lo q tengo q hacer para q luego de ocultar el menu aparezca el otro viewController
            
            //var q se va a usar para tomar el nombre de la celda y pasarlo como parametro a la func showControllerMenu
            let opcion = self.menuOption[indexPath.item - 1].name
            
            //lamando a las funciones que presenta a los ViewControllers del menu
            self.mainViewController?.showControllersMenu(NameViewcontroller: opcion)
        }
    }

}
