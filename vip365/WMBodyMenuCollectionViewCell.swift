//
//  WMBodyMenuCollectionViewCell.swift
//  vip365
//
//  Created by Wilmer Mendoza on 3/10/17.
//  Copyright © 2017 Andres Rodriguez. All rights reserved.
//

import UIKit

class WMBodyMenuCollectionViewCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.rgb(red: 238, green: 238, blue: 238)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var setting : WMMenu?{
        //asignandole los valores a las celdas dinamicamente
        didSet {
            actionNameLabel.text = setting!.name
        }
        
    }
    
    let actionNameLabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.clipsToBounds = true
        label.text = "Pagos"
        label.font = UIFont.systemFont(ofSize: 14, weight: UIFontWeightSemibold)
        label.textColor = .black
        return label
    }()
    
    
    func setupViews(){
        
        addSubview(actionNameLabel)
        
        actionNameLabel.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        actionNameLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 12).isActive = true
        actionNameLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -8).isActive = true
    }
    
}
