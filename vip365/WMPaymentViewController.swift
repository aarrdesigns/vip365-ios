//
//  WMPaymentViewController.swift
//  vip365
//
//  Created by Wilmer Mendoza on 3/10/17.
//  Copyright © 2017 Andres Rodriguez. All rights reserved.
//

import UIKit

class WMPaymentViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let cellId1 = "cellId1"
    let cellId2 = "cellId2"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        settingTableView()
        settingNavbar()

    }

    
    fileprivate func settingTableView(){
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(WMPaymentTableViewCell1.self, forCellReuseIdentifier: cellId1)
        tableView.register(WMPaymentTableViewCell2.self, forCellReuseIdentifier: cellId2)
    }
    
    fileprivate func settingNavbar(){
//        navigationController?.navigationBar.barTintColor = .black
//        navigationController?.navigationBar.isTranslucent = false
//        UINavigationBar.appearance().tintColor = UIColor.white
//        
//        let cancelButton = UIBarButtonItem(title: "X", style: .plain, target: self, action: #selector(cancel))
//        navigationItem.leftBarButtonItem = cancelButton
//        
//        let titleView = UIView(frame: CGRect(x: 8, y: 44, width: view.frame.width, height: 30))
//        titleView.backgroundColor = .red
//        
//        self.navigationItem.titleView = titleView

    }
    
    func cancel(){
        print("hola")
      
    }

    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
}


extension WMPaymentViewController: UITableViewDelegate,UITableViewDataSource{

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0{
            return "METODOS DE PAGO"
        }else{
            return "promociones"
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 3
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell1 = tableView.dequeueReusableCell(withIdentifier: cellId1) as! WMPaymentTableViewCell1
        let cell2 = tableView.dequeueReusableCell(withIdentifier: cellId2) as! WMPaymentTableViewCell2
        
        switch indexPath.section {
        case 0:
            
            switch indexPath.row {
            case 0,1:
                return cell1
            default:
                return cell2
            }
            
        default:
            return cell2
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.section{
        case 0:
            
            switch indexPath.row {
                
            case 0,1:
                let vc = UIStoryboard(name: "Home", bundle: Bundle.main).instantiateViewController(withIdentifier: "WMDetailMethoodPaymentViewController") as! WMDetailMethoodPaymentViewController
                
                self.present(vc, animated: true, completion: nil)
                break
            default:
                break
            }
            break
            
        default:
            break
        }
        
    }

}
