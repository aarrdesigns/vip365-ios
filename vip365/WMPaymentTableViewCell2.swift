//
//  WMPaymentTableViewCell2.swift
//  vip365
//
//  Created by Wilmer Mendoza on 3/10/17.
//  Copyright © 2017 Andres Rodriguez. All rights reserved.
//

import UIKit

class WMPaymentTableViewCell2: UITableViewCell {

    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        setupView()
    }
        
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    let numberCardLabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.clipsToBounds = true
        label.text = "Agregar otro metodo de pago"
        label.textColor = UIColor.primaryColor()
        return label
    }()
    
    let separatorView : UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.separatorCell()
        return view
    }()
        
        
    func setupView(){
            
        addSubview(numberCardLabel)
        addSubview(separatorView)
        
        numberCardLabel.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        numberCardLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 12).isActive = true
        
        separatorView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        separatorView.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        separatorView.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        separatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
    }
    
}
