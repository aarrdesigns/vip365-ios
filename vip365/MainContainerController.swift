//
//  File.swift
//  vip365
//
//  Created by Andres Rodriguez on 9/30/17.
//  Copyright © 2017 Andres Rodriguez. All rights reserved.
//

import UIKit

class MainContainerController: UIViewController{
    
    @IBAction func onMenuTapped() {
        print("Menu tapped")
        NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil)
    }

}
