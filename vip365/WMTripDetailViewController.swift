//
//  WMTripDetailViewController.swift
//  vip365
//
//  Created by Wilmer Mendoza on 6/10/17.
//  Copyright © 2017 Andres Rodriguez. All rights reserved.
//

import UIKit

class WMTripDetailViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let cellId1 = "cellId1"
    let cellId2 = "cellId2"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        settingTableView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func settingTableView(){
    
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(WMTripDetailHeaderTableViewCell.self, forCellReuseIdentifier: cellId1)
        tableView.register(WMTripDetailStreetTableViewCell.self, forCellReuseIdentifier: cellId2)
    }
    

    @IBAction func goBack(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

}


extension WMTripDetailViewController: UITableViewDelegate,UITableViewDataSource{

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell1 = tableView.dequeueReusableCell(withIdentifier: cellId1) as! WMTripDetailHeaderTableViewCell
        let cell2 = tableView.dequeueReusableCell(withIdentifier: cellId2) as! WMTripDetailStreetTableViewCell
        
        switch indexPath.row {
        case 0:
            return cell1
        default:
            return cell2
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 240
        default:
            return 60
        }
    }

}
