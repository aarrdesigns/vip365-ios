//
//  WMTripsViewController.swift
//  vip365
//
//  Created by Wilmer Mendoza on 5/10/17.
//  Copyright © 2017 Andres Rodriguez. All rights reserved.
//

import UIKit

class WMTripsViewController: UIViewController  {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var nabView: UIView!
    @IBOutlet weak var containerViewOne: UIView!
    @IBOutlet weak var containerViewTwo: UIView!
    @IBOutlet weak var containerViewThree: UIView!
    
    var horizontalBar = UIView()
    
    let cellId1 = "cellId1"
    let cellIdCollectionView = "cellIdColl"
    
    let titleNames = ["Past        ","Upcoming","Family"]
    
    var horizontalBarConstraint: NSLayoutConstraint?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        settingCollectionView()
        setupHorizontalBar()
        settingContainers()
    }
    

    
    func settingCollectionView(){
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.register(WMTripMenuCollectionViewCell.self, forCellWithReuseIdentifier: cellIdCollectionView)
    }
    
    func setupHorizontalBar(){
        horizontalBar.translatesAutoresizingMaskIntoConstraints = false
        horizontalBar.backgroundColor = UIColor.rgb(red: 66, green: 160, blue: 196)
        
        self.view.addSubview(horizontalBar)
        
        horizontalBarConstraint = horizontalBar.leftAnchor.constraint(equalTo: self.view.leftAnchor )
        horizontalBarConstraint?.isActive = true
        horizontalBar.bottomAnchor.constraint(equalTo: nabView.bottomAnchor, constant: 0).isActive = true
        horizontalBar.widthAnchor.constraint(equalTo: nabView.widthAnchor, multiplier: 1/3).isActive = true
        horizontalBar.heightAnchor.constraint(equalToConstant: 4).isActive = true

    }
    
    func settingContainers(){
        containerViewOne.alpha = 1
        containerViewTwo.alpha = 0
        containerViewThree.alpha = 0
    }
    
    func showContainer(index: Int){
        
        switch index {
        case 0:
            
            UIView.animate(withDuration: 0.75, animations: { 
                self.containerViewOne.alpha = 1
                self.containerViewTwo.alpha = 0
                self.containerViewThree.alpha = 0
            }, completion: nil)
            break
        case 1:
            UIView.animate(withDuration: 0.75, animations: {
                self.containerViewOne.alpha = 0
                self.containerViewTwo.alpha = 1
                self.containerViewThree.alpha = 0
            }, completion: nil)
            break
        case 2:
            UIView.animate(withDuration: 0.75, animations: {
                self.containerViewOne.alpha = 0
                self.containerViewTwo.alpha = 0
                self.containerViewThree.alpha = 1
            }, completion: nil)
            
            break
        default:
            break
        }
    
    }
    
    @IBAction func goBack(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
}



extension WMTripsViewController: UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource{
    
    //retorna 4 elementos porque seran 4 botones q queremos mostrar
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    //creando las 4 celdas y hacemos casting a MenuCell para poder setear el elemento imageView de ese clase
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdCollectionView, for: indexPath) as! WMTripMenuCollectionViewCell
        
        cell.titleLabel.text = titleNames[indexPath.row]
        cell.tintColor = UIColor.rgb(red: 91, green: 14, blue: 13)
        
        return cell
    }
    
    // definiendo el tamaño de las celdas y el width se divide entre 4 para q hayan 4 por fila
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/3, height: collectionView.frame.height)
    }
    
    
    //seteando el espacio entre celdas
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let x = CGFloat(indexPath.item) * view.frame.width/3
        horizontalBarConstraint?.constant = x
        
        UIView.animate(withDuration: 0.75, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: { 
            self.view.layoutIfNeeded()
        }, completion: nil)
    
        showContainer(index: indexPath.item)
    }


}
