//
//  MenuBar.swift
//  vip365
//
//  Created by Wilmer Mendoza on 5/10/17.
//  Copyright © 2017 Andres Rodriguez. All rights reserved.
//

import UIKit


class WMTripMenuCollectionViewCell: UICollectionViewCell {

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let titleLabel : UILabel = {
        
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.clipsToBounds = true
        label.textColor = .white
        return label
    }()
    
    //esta es una variable q indica cuando se ha hecho tap sobre una celda
    override var isHighlighted: Bool{
        didSet {
            //aqui pintamos el icono de blanco si se ha hecho tap en la celda si no la dejamos negra
            titleLabel.tintColor = isHighlighted ? UIColor.white : UIColor.rgb(red: 91, green: 14, blue: 13)
        }
    }
    
    //
    override var isSelected: Bool{
        didSet {
            //aqui pintamos el icono de blanco si la celda esta selecccionada si no la dejamos negra
            titleLabel.tintColor = isSelected ? UIColor.white : UIColor.rgb(red: 91, green: 14, blue: 13)
        }
        
    }
    
    func setupViews() {
        addSubview(titleLabel)
        
        //centrando las imagenes en el centro de su celda es decir centrandolas en el eje x y en el y
        addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .centerY , relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        
    }

}
