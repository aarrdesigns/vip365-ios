//
//  WMHeaderMenuCollectionViewCell.swift
//  vip365
//
//  Created by Wilmer Mendoza on 3/10/17.
//  Copyright © 2017 Andres Rodriguez. All rights reserved.
//

import UIKit

class WMHeaderMenuCollectionViewCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.primaryColor()
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    let circleView : UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true
        view.backgroundColor = UIColor.rgb(red: 234, green: 183, blue: 65)
        view.layer.cornerRadius = 26
        return view
    }()
    
    let imageProfile : UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "profile")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 24
        return imageView
    }()
    
    let namelabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.clipsToBounds = true
        label.text = "Wilmer Mendoza"
        label.textColor = .white
        return label
    }()
    
    let ratelabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.clipsToBounds = true
        label.text = "Calificacion"
        label.textColor = .white
        return label
    }()
    
    
    func setupView(){
        
        addSubview(circleView)
        addSubview(imageProfile)
        addSubview(namelabel)
        addSubview(ratelabel)
        
        circleView.topAnchor.constraint(equalTo: topAnchor, constant: 30).isActive = true
        circleView.leftAnchor.constraint(equalTo: leftAnchor, constant: 12).isActive = true
        circleView.widthAnchor.constraint(equalToConstant: 52).isActive = true
        circleView.heightAnchor.constraint(equalToConstant: 52).isActive = true
        
        imageProfile.centerYAnchor.constraint(equalTo: circleView.centerYAnchor, constant: 0).isActive = true
        imageProfile.centerXAnchor.constraint(equalTo: circleView.centerXAnchor, constant: 0).isActive = true
        imageProfile.heightAnchor.constraint(equalToConstant: 48).isActive = true
        imageProfile.widthAnchor.constraint(equalToConstant: 48).isActive = true
        
        namelabel.centerYAnchor.constraint(equalTo: circleView.centerYAnchor, constant: -15).isActive = true
        namelabel.leftAnchor.constraint(equalTo: circleView.rightAnchor, constant: 10).isActive = true
        namelabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -8).isActive = true
        
        ratelabel.centerYAnchor.constraint(equalTo: circleView.centerYAnchor, constant: 15).isActive = true
        ratelabel.leftAnchor.constraint(equalTo: namelabel.leftAnchor, constant: 0).isActive = true
        ratelabel.rightAnchor.constraint(equalTo: namelabel.rightAnchor, constant: 0).isActive = true
    
    }
    
    
}
