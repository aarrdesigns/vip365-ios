//
//  WMDetailMethoodPaymentViewController.swift
//  vip365
//
//  Created by Wilmer Mendoza on 4/10/17.
//  Copyright © 2017 Andres Rodriguez. All rights reserved.
//

import UIKit

class WMDetailMethoodPaymentViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    let cellId1 = "cellId1"
    

    override func viewDidLoad() {
        super.viewDidLoad()

        settingTableView()
        
        // Do any additional setup after loading the view.
    }
    
    func settingTableView(){
        
        tableView.delegate = self
        tableView.dataSource = self
    
        tableView.register(WMDetailMethoodPaymentTableViewCell.self, forCellReuseIdentifier: cellId1)
    }

    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }

}


extension WMDetailMethoodPaymentViewController: UITableViewDelegate,UITableViewDataSource{

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId1) as! WMDetailMethoodPaymentTableViewCell
   
        switch indexPath.row {
        case 0:
            cell.titleLabel.text = "Numero de tarjeta"
            break
        default:
            cell.titleLabel.text = "Fecha de vencimiento"
            cell.numberCardLabel.text = "09/2018"
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    


}

