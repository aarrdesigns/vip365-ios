//
//  Extensions.swift
//  vip365
//
//  Created by Wilmer Mendoza on 3/10/17.
//  Copyright © 2017 Andres Rodriguez. All rights reserved.
//

import UIKit


extension UIColor{
    
    open class func rgb(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor{
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1)
    }
    
    static func separatorCell() -> UIColor{
        return UIColor.rgb(red: 230, green: 230, blue: 230)
    }
    
    static func primaryColor() -> UIColor{
        return UIColor.rgb(red: 27, green: 58, blue: 104)
    }
    
    static func placeHolderColor() -> UIColor{
        return UIColor.rgb(red: 167, green: 167, blue: 175)
    }
    
}


extension UIViewController{
    
    func showControllersMenu(NameViewcontroller name : String){
        
        let vc1 = UIStoryboard(name: "Home", bundle: Bundle.main).instantiateViewController(withIdentifier: "WMPaymentViewController") as! WMPaymentViewController
        let vc2 = UIStoryboard(name: "Home", bundle: Bundle.main).instantiateViewController(withIdentifier: "WMTripsViewController") as! WMTripsViewController
        
        switch name {
        case "Pagos":
            present(vc1, animated: false, completion: nil)
            break
        case "Tus viajes":
            present(vc2, animated: false, completion: nil)
            break
        default:
            break
        }
    }

}



extension UIView {
    
    // OUTPUT 1
    func dropShadow(scale: Bool = true) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.shadowRadius = 1
        
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    // OUTPUT 2
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = offSet
        self.layer.shadowRadius = radius
        
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    
    func addConstraintWithFormat(_ format : String, views : UIView...){
            
        var viewsDitcionary = [String : UIView]()
            
        for(index, view) in views.enumerated(){
            let key = "v\(index)"
                
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDitcionary[key] = view
                
        }
            
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDitcionary ))
            
    }
        
}
