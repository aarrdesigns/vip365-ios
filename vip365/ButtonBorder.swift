//
//  btnInicio.swift
//  vip365
//
//  Created by Andres Rodriguez on 9/29/17.
//  Copyright © 2017 Andres Rodriguez. All rights reserved.
//

import UIKit

class ButtonBorder: UIButton{
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let swiftColor = hexStringToUIColor(hex: "#1D396C")
        layer.borderWidth = 2/UIScreen.main.nativeScale
        layer.borderColor = swiftColor.cgColor
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.borderColor = tintColor.cgColor
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
