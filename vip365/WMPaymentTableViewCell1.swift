//
//  WMPaymentTableViewCell1.swift
//  vip365
//
//  Created by Wilmer Mendoza on 3/10/17.
//  Copyright © 2017 Andres Rodriguez. All rights reserved.
//

import UIKit

class WMPaymentTableViewCell1: UITableViewCell {

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        accessoryType = .disclosureIndicator
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    let imageCard : UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.clipsToBounds = true
        image.image = #imageLiteral(resourceName: "credit_card")
        image.contentMode = .scaleAspectFill
        return image
    }()
    
    let numberCardLabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.clipsToBounds = true
        label.text = "••••9959"
        return label
    }()
    
    let separatorView : UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.separatorCell()
        return view
    }()
    
    
    func setupView(){
        
        addSubview(imageCard)
        addSubview(numberCardLabel)
        addSubview(separatorView)
        
        imageCard.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
        imageCard.leftAnchor.constraint(equalTo: leftAnchor, constant: 12).isActive = true
        imageCard.widthAnchor.constraint(equalToConstant: 15).isActive = true
        imageCard.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        numberCardLabel.centerYAnchor.constraint(equalTo: imageCard.centerYAnchor, constant: 0).isActive = true
        numberCardLabel.leftAnchor.constraint(equalTo: imageCard.rightAnchor, constant: 8).isActive = true
        
        separatorView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        separatorView.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        separatorView.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        separatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
    }

}
