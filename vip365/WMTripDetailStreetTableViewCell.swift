//
//  WMTripDetailStreetTableViewCell.swift
//  vip365
//
//  Created by Wilmer Mendoza on 6/10/17.
//  Copyright © 2017 Andres Rodriguez. All rights reserved.
//

import UIKit

class WMTripDetailStreetTableViewCell: UITableViewCell {

    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let circleView : UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true
        view.backgroundColor = .black
        view.layer.cornerRadius = 5
        return view
    }()
    
    let squareView : UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true
        view.backgroundColor = .black
        return view
    }()
    
    let verticalLineView : UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true
        view.backgroundColor = .black
        return view
    }()
    
    let initialAddressLabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Calle espinoza 5 a san jose 8030, san diego carabobo"
        label.textColor = UIColor.rgb(red: 168, green: 169, blue: 176)
        label.font = UIFont.systemFont(ofSize: 17)
        return label
    }()
    
    let finalAddressLabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Calle camino viejo a san jose 8030, san diego carabobo"
        label.textColor = UIColor.rgb(red: 168, green: 169, blue: 176)
        label.font = UIFont.systemFont(ofSize: 17)
        return label
    }()
    
    
    func setupViews(){
        
        addSubview(circleView)
        addSubview(verticalLineView)
        addSubview(squareView)
        addSubview(initialAddressLabel)
        addSubview(finalAddressLabel)
        
        circleView.topAnchor.constraint(equalTo: topAnchor, constant: 10).isActive = true
        circleView.leftAnchor.constraint(equalTo: leftAnchor, constant: 24).isActive = true
        circleView.widthAnchor.constraint(equalToConstant: 10).isActive = true
        circleView.heightAnchor.constraint(equalToConstant: 10).isActive = true
        
        verticalLineView.topAnchor.constraint(equalTo: circleView.bottomAnchor, constant: 2).isActive = true
        verticalLineView.centerXAnchor.constraint(equalTo: circleView.centerXAnchor, constant: 0).isActive = true
        verticalLineView.heightAnchor.constraint(equalToConstant: 20).isActive = true
        verticalLineView.widthAnchor.constraint(equalToConstant: 1).isActive = true
        
        squareView.topAnchor.constraint(equalTo: verticalLineView.bottomAnchor, constant: 2).isActive = true
        squareView.centerXAnchor.constraint(equalTo: circleView.centerXAnchor, constant: 0).isActive = true
        squareView.heightAnchor.constraint(equalToConstant: 10).isActive = true
        squareView.widthAnchor.constraint(equalToConstant: 10).isActive = true
        
        initialAddressLabel.topAnchor.constraint(equalTo: squareView.topAnchor, constant: -5).isActive = true
        initialAddressLabel.leftAnchor.constraint(equalTo: squareView.rightAnchor, constant: 12).isActive = true
        initialAddressLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -8).isActive = true
        
        finalAddressLabel.topAnchor.constraint(equalTo: circleView.topAnchor, constant: -5).isActive = true
        finalAddressLabel.leftAnchor.constraint(equalTo: circleView.rightAnchor, constant: 12).isActive = true
        finalAddressLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -8).isActive = true
        
    }
    
}
