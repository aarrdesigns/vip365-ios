//
//  MainMenuController.swift
//  vip365
//
//  Created by Andres Rodriguez on 9/29/17.
//  Copyright © 2017 Andres Rodriguez. All rights reserved.
//

import UIKit

class MainMenuController: UIViewController{
    
    @IBOutlet weak var sideMenuConstraint: NSLayoutConstraint!
    var sideMenuOpen = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        // Do any additional setup after loading the view, typically from a nib.
    
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(toggleSideMenu),
                                               name: NSNotification.Name("ToggleSideMenu"),
                                               object: nil)
    }
    
    @objc func toggleSideMenu(){
        if sideMenuOpen{
            sideMenuConstraint.constant = -240
            sideMenuOpen = false
        }else{
            sideMenuConstraint.constant = 0
            sideMenuOpen = true
        }
    }
}
