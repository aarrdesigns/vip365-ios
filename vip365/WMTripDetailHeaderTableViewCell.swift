//
//  WMTripDetailTableViewCell.swift
//  vip365
//
//  Created by Wilmer Mendoza on 6/10/17.
//  Copyright © 2017 Andres Rodriguez. All rights reserved.
//

import UIKit

class WMTripDetailHeaderTableViewCell: UITableViewCell {

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    let imageMap : UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.clipsToBounds = true
        image.image = #imageLiteral(resourceName: "map")
        image.contentMode = .scaleAspectFill
        return image
    }()
    
    let whiteView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true
        view.backgroundColor = .white
        return view
    }()
    
    let labelDate : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "10/21/17 12:38pm"
        return label
    }()
    
    let labelModelCar : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Ford focus"
        label.textColor = UIColor.rgb(red: 168, green: 169, blue: 176)
        label.font = UIFont.systemFont(ofSize: 17)
        return label
    }()
    
    let labelPrice : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "MX$60.80"
        return label
    }()
    
    func setupViews(){
    
        addSubview(imageMap)
        addSubview(whiteView)
        addSubview(labelDate)
        addSubview(labelModelCar)
        addSubview(labelPrice)
        
        imageMap.topAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
        imageMap.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        imageMap.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        imageMap.heightAnchor.constraint(equalToConstant: 170).isActive = true
        
        whiteView.topAnchor.constraint(equalTo: imageMap.bottomAnchor, constant: 0).isActive = true
        whiteView.leftAnchor.constraint(equalTo: imageMap.leftAnchor, constant: 0).isActive = true
        whiteView.rightAnchor.constraint(equalTo: imageMap.rightAnchor, constant: 0).isActive = true
        whiteView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        
        labelDate.centerYAnchor.constraint(equalTo: whiteView.centerYAnchor, constant: -10).isActive = true
        labelDate.leftAnchor.constraint(equalTo: whiteView.leftAnchor, constant: 24).isActive = true
        
        labelModelCar.topAnchor.constraint(equalTo: labelDate.bottomAnchor, constant: 2).isActive = true
        labelModelCar.leftAnchor.constraint(equalTo: labelDate.leftAnchor, constant: 0).isActive = true
        
        labelPrice.rightAnchor.constraint(equalTo: whiteView.rightAnchor, constant: -24).isActive = true
        labelPrice.centerYAnchor.constraint(equalTo: whiteView.centerYAnchor, constant: -10).isActive = true
    }
    
}
