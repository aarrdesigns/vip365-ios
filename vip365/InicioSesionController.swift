//
//  InicioSesionController.swift
//  vip365
//
//  Created by Andres Rodriguez on 9/29/17.
//  Copyright © 2017 Andres Rodriguez. All rights reserved.
//

import UIKit

class InicioSesionController: UIViewController{
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.title = "Inicia sesión"
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func goToMainView(_ sender: UIButton) {
        let vc = UIStoryboard(name: "Home", bundle: Bundle.main).instantiateInitialViewController()
        
        self.present(vc!, animated: true, completion: nil)
        
    }
}
